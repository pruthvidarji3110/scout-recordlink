import React from "react";
import { Modal, Button } from "react-bootstrap";

//component import
import * as inputFun from '../common-components/FormInputFunctions';

export default function EditUserProfileModel(props) {
  return (
    <div className="maplayout-page">
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className="app-model location-profile-model"
      id="drpro-select">
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter " className="w-100  pb-0 blue-title-h6">
          Edit Profile
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="app-model-body">
      <form>
            <div className="row ">
              <div className=" col-sm-6 ">
                <div class="form-group app-ctrl has-value">
                  <label class="required">First Name </label>
                  <input type="text" className="form-control" defaultValue="John" onChange={inputFun.checkInputHasValue}/>
                </div>
              </div>
              <div className=" col-sm-6 ">
                <div class="form-group app-ctrl has-value">
                  <label class="required">Last Name</label>
                  <input type="text" className="form-control" defaultValue="Doe" onChange={inputFun.checkInputHasValue}/>
                </div>
              </div>
              <div className=" col-sm-6 ">
                <div class="form-group app-ctrl has-value">
                  <label class="required">Specialty</label>
                  <input type="text" className="form-control" defaultValue="General Dentist" onChange={inputFun.checkInputHasValue}/>
                </div>
              </div>
              <div className=" col-sm-6 ">
                <div class="form-group app-ctrl has-value">
                  <label class="required">User Name</label>
                  <input type="text" className="form-control" defaultValue="azmax" onChange={inputFun.checkInputHasValue}/>
                </div>
              </div>
              <div className=" col-sm-6 ">
                <div class="form-group app-ctrl has-value">
                  <label class="required">Email</label>
                  <input type="email" className="form-control" defaultValue="Johndoe@gmail.com" onChange={inputFun.checkInputHasValue}/>
                </div>
              </div>
             
            </div>
              <div className="btn-box d-flex justify-content-center">
                <button type="button" className="btn app-btn lightgray-btn large-btn mr-2"  onClick={props.onHide}>cancel</button>
                <button type="button" className="btn app-btn large-btn ">Update</button>
              </div>
            
          </form>

      </Modal.Body>
     
    </Modal>
    </div>
  );
}



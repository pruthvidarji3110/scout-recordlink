import { Route, Switch } from "react-router-dom";
import "./App.scss";
// component imports
import IntegrationDirectoryPage from "./app-pages/IntegrationDirectoryPage";
import IntegrationLocationPage from "./app-pages/IntegrationLocationPage";
import ManageConnectionPage from "./app-pages/ManageConnectionPage";
import BillingPage from "./app-pages/BillingPage";
import ProfilePage from "./app-pages/ProfilePage";
import Setting from "./app-pages/Setting";
import PaymentMethodPage from "./app-pages/PaymentMethodPage";
import ErrorPage from "./app-pages/ErrorPage";
function App() {
  return (
    <Switch>
      <Route exact path="/" component={IntegrationDirectoryPage} />
      <Route exact path="/dentrix" component={IntegrationLocationPage} />
      <Route exact path="/dentrix/manage" component={ManageConnectionPage} />
      <Route exact path="/billing" component={BillingPage} />
      <Route
        exact
        path="/billing/paymentmethod"
        component={PaymentMethodPage}
      />
      <Route exact path="/profile" component={ProfilePage} />
      <Route exact path="/setting" component={Setting} />
      <Route component={ErrorPage} />
    </Switch>
  );
}
export default App;
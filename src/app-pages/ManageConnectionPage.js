import React from 'react';
import { NavLink } from 'react-router-dom';
import { Player } from 'video-react';
// component imports
import AppSidebarNavbar from '../common-components/AppSidebarNavbar';
//images import
import NoteEditIcon from '../assets/images/noteEdit-icon-black.svg';
import HIPPAlogo from '../assets/images/HIPAA-logo.png';
import DownloadIcon from '../assets/images/download-icon-white.svg';
import ReloadIcon from '../assets/images/reloadicon-white.svg';
import howItWorksImg from '../assets/images/howitworks-img.png';

export default function ManageConnectionPage() {
    return (
        <div className="main-wrapper">
            <AppSidebarNavbar activeTabsIntegration={true} />
            <main>
                <div className="app-container">
                    <nav aria-label="breadcrumb" className="app-breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item "><NavLink exact to="/" >Integrations </NavLink></li>
                            <li className="breadcrumb-item "><NavLink exact to="/dentrix" >Dentrix </NavLink></li>
                            <li className="breadcrumb-item active"><a >Manage </a></li>
                        </ol>
                    </nav>
                    <p className="header-note">Follow the steps below to install our proprietary data Sync tool on your system.</p>
                    <div className="app-content">
                        <div className="white-container manage-connection-page">
                            <section className="sidebanner-section">
                                <div className="text-wrapper d-flex justify-content-center flex-column">
                                    <h2>Installation Instructions here</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <div className="btn-box d-flex ">
                                        <div className="btn-wrapper mr-2">
                                            <span className="step-count">STEP 01</span>
                                            <button className="btn app-btn text-uppercase">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 22">
                                                    <path fill="#fff" d="M10.908 1.06a.486.486 0 00-.35-.156h-7.84A2.652 2.652 0 00.077 3.54v14.826a2.652 2.652 0 002.64 2.636H13.36c1.447 0 2.641-1.19 2.641-2.636V6.594a.522.522 0 00-.14-.34l-4.952-5.195zm.14 1.558l3.316 3.48h-2.155c-.641 0-1.16-.514-1.16-1.155V2.618zM13.36 20.03H2.718c-.908 0-1.67-.752-1.67-1.665V3.54c0-.908.757-1.665 1.67-1.665h7.36v3.068c0 1.18.951 2.126 2.13 2.126h2.821v11.297c0 .913-.757 1.665-1.67 1.665z" />
                                                    <path fill="#fff" d="M12.034 16.682h-7.99a.487.487 0 00-.486.485c0 .267.218.486.485.486h7.996a.487.487 0 00.485-.486.488.488 0 00-.49-.485zM7.684 14.939a.489.489 0 00.354.155.488.488 0 00.355-.155l2.845-3.054a.484.484 0 10-.71-.66l-2.004 2.15v-5.3a.487.487 0 00-.486-.486.487.487 0 00-.485.485v5.302l-2-2.151a.484.484 0 10-.709.66l2.84 3.054z" />
                                                </svg> 
                                                <span>Installation Guide</span>
                                            </button>
                                        </div>
                                        <div className="btn-wrapper">
                                            <span className="step-count">STEP 02</span>
                                            <button className="btn app-btn text-uppercase">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 22">
                                                    <path fill="#fff" d="M10.908 1.06a.486.486 0 00-.35-.156h-7.84A2.652 2.652 0 00.077 3.54v14.826a2.652 2.652 0 002.64 2.636H13.36c1.447 0 2.641-1.19 2.641-2.636V6.594a.522.522 0 00-.14-.34l-4.952-5.195zm.14 1.558l3.316 3.48h-2.155c-.641 0-1.16-.514-1.16-1.155V2.618zM13.36 20.03H2.718c-.908 0-1.67-.752-1.67-1.665V3.54c0-.908.757-1.665 1.67-1.665h7.36v3.068c0 1.18.951 2.126 2.13 2.126h2.821v11.297c0 .913-.757 1.665-1.67 1.665z" />
                                                    <path fill="#fff" d="M10.658 14.38a.375.375 0 01.104-.52l1.197-.797a.379.379 0 01.523.102l.8 1.2a.375.375 0 01-.624.415l-.24-.36a4.374 4.374 0 01-8.255.702.375.375 0 11.687-.3 3.625 3.625 0 006.866-.696l-.538.359a.375.375 0 01-.52-.105zM4.1 13.74a.38.38 0 01-.239-.16l-.8-1.199a.378.378 0 01.071-.494.378.378 0 01.554.079l.24.36A4.38 4.38 0 018.17 9a4.367 4.367 0 014.03 2.67.375.375 0 11-.69.293A3.619 3.619 0 008.17 9.75a3.63 3.63 0 00-3.544 2.87l.538-.359a.375.375 0 01.416.624l-1.197.798a.377.377 0 01-.284.057z" />
                                                </svg> 
                                                <span>Download Sync</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="video-wrapper">
                                    <Player>
                                        <source src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4" />
                                    </Player>
                                </div>
                            </section>
                            <section className="howitworks-section">
                                <h2 className="section-header">How it works?</h2>
                                <div className="howitworks-container d-flex align-items-center justify-content-between">
                                    <div className="img-wrapper">
                                        <img src={howItWorksImg} alt="Banner Image" />
                                    </div>
                                    <div className="text-wrapper d-flex justify-content-center flex-column">
                                        <h2>Lorem ipsum <br />dummy text display</h2>
                                        <p>Lorem Ipsum is simply dummy text of the data is synced from their PMS to the app</p>
                                    </div>
                                </div>
                            </section>
                            <div className="row app-box-wrapper mt-2">
                                <div className="box-col col-xs-12 col-sm-12 col-lg-12 col-xl-9">
                                    <div className="app-box info-box">
                                        <div className="left-box">
                                            <h2 className="info-title">Pay for an expert! </h2>
                                            <p>Need a little assistance with installing the integration tool? No problem! We have experts who can save you time, and get you up and going quickly!</p>
                                        </div>
                                        <div className="right-box">
                                            <div className="amt-info">
                                                <img src={NoteEditIcon} alt="Icon" />
                                                <strong>$149</strong>
                                                <span>One time</span>
                                            </div>
                                            <div className="btn-box">
                                                <button className="btn app-btn small-btn text-uppercase">
                                                    Learn More
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="box-col col-xs-12 col-sm-12 col-lg-12 col-xl-3">
                                    <div className="hippa-img-box d-flex align-items-center justify-content-center">
                                        <img src={HIPPAlogo} alt="HIPPA Logo" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    );
}
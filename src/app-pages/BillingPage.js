import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import DatePicker from "react-datepicker";
import FeatherIcon from 'feather-icons-react';
// component imports
import AppSidebarNavbar from '../common-components/AppSidebarNavbar';
import AddLocationModel from '../models/AddLocationModel';
//images import
import DownArrowIcon from '../assets/images/icon-donarrow-green.svg';
import PrintIcon from '../assets/images/icon-print-green.svg';
import PaymentCardImg from '../assets/images/paymentcard-img.svg';
import WifiSignalImg from '../assets/images/wifi-signal-img.svg';
import VisaCardImg from '../assets/images/visa-card-img.svg';
import CvvImg from '../assets/images/cvv-img.svg';

export default function BillingPage() {
    const [AddLocationModelShow, SetAddLocationModel,] = React.useState(false);
    const [expDate, setExpDate] = useState(new Date("02/02/2023"));
    return (
        <div className="main-wrapper">
            <AppSidebarNavbar activeTabsBilling={true} />
            <main>
                <div className="app-container">
                    <nav aria-label="breadcrumb" className="app-breadcrumb req-mar">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active" aria-current="page">Billings</li>
                        </ol>
                    </nav>
                    <div className="app-content">
                        <div className="billing-page">
                            <div className="grid-section">
                                <div className="white-container py-0">
                                    <div className="action-row d-flex justify-content-end">
                                        <div className="left-box d-flex">
                                            <div className="form-group app-select">
                                                <select className="form-control">
                                                    <option>Select a Month</option>
                                                    <option>January</option>
                                                    <option>February</option>
                                                    <option>March</option>
                                                    <option>April</option>
                                                    <option>May</option>
                                                    <option>June</option>
                                                    <option>July</option>
                                                    <option>August</option>
                                                    <option>September</option>
                                                    <option>October</option>
                                                    <option>November</option>
                                                    <option>December</option>
                                                </select>
                                            </div>
                                            <div className="form-group app-select">
                                                <select className="form-control">
                                                    <option>Select a Year</option>
                                                    <option>2021</option>
                                                    <option>2020</option>
                                                    <option>2019</option>
                                                    <option>2018</option>
                                                    <option>2017</option>
                                                    <option>2016</option>
                                                    <option>2015</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="right-box d-flex align-items-start">
                                            {/* <div className="form-group input-group app-search">
                                                <input type="text" className="form-control" placeholder="Search" />
                                                <span className="search-icon">
                                                    <i className="fa fa-search"></i>
                                                </span>
                                                <button className="btn search-btn">
                                                    <span>Search</span>
                                                    <i className="fa fa-search"></i>
                                                </button>
                                            </div> */}
                                            <NavLink exact to="/billing/paymentmethod" className="btn app-btn small-btn text-uppercase">Payment Method</NavLink>
                                            {/* <button className="btn app-btn small-btn text-uppercase">Payment Method</button> */}
                                        </div>
                                    </div>
                                    <div className="app-table">
                                        <div className="table-responsive">
                                            <table className="table table-hover table-borderless invoice-grid">
                                                <thead>
                                                    <tr>
                                                        <th className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chkall" />
                                                                <label className="custom-control-label" htmlFor="chkall"></label>
                                                            </div>
                                                        </th>
                                                        <th>
                                                            Invoice Date
                                                        </th>

                                                        <th>
                                                            Invoice Total
                                                        </th>
                                                        <th>
                                                            Invoice Status
                                                        </th>
                                                        <th>
                                                            Actions
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk1" />
                                                                <label className="custom-control-label" htmlFor="chk1"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Aug, 2020
                                                        </td>

                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                            <span className="app-badge app-badge-success">Paid</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk2" />
                                                                <label className="custom-control-label" htmlFor="chk2"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Sep, 2020
                                                        </td>

                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                            <span className="app-badge app-badge-danger">Refund</span>

                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk3" />
                                                                <label className="custom-control-label" htmlFor="chk3"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Oct, 2020
                                                        </td>
                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                            <span className="app-badge app-badge-success">Paid</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk4" />
                                                                <label className="custom-control-label" htmlFor="chk4"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Aug, 2020
                                                        </td>
                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                            <span className="app-badge app-badge-danger">Refund</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk5" />
                                                                <label className="custom-control-label" htmlFor="chk5"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Sep, 2020
                                                        </td>
                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                            <span className="app-badge app-badge-success">Paid</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk6" />
                                                                <label className="custom-control-label" htmlFor="chk6"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Oct, 2020
                                                        </td>
                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                            <span className="app-badge app-badge-danger">Refund</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk7" />
                                                                <label className="custom-control-label" htmlFor="chk7"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Aug, 2020
                                                        </td>
                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                            <span className="app-badge app-badge-success">Paid</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk8" />
                                                                <label className="custom-control-label" htmlFor="chk8"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Sep, 2020
                                                        </td>
                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                            <span className="app-badge app-badge-danger">Refund</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk11" />
                                                                <label className="custom-control-label" htmlFor="chk11"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Oct, 2020
                                                        </td>
                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                        <span className="app-badge app-badge-success">Paid</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk9" />
                                                                <label className="custom-control-label" htmlFor="chk9"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Sep, 2020
                                                        </td>
                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                            <span className="app-badge app-badge-danger">Refund</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td className="chk-col">
                                                            <div className="custom-control custom-checkbox app-checkbox">
                                                                <input type="checkbox" className="custom-control-input" id="chk10" />
                                                                <label className="custom-control-label" htmlFor="chk10"></label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            01 Oct, 2020
                                                        </td>

                                                        <td>
                                                            $ 1,648
                                                        </td>
                                                        <td>
                                                        <span className="app-badge app-badge-success">Paid</span>
                                                        </td>
                                                        <td>
                                                            {/* <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="arrow-down" size="20" /> 
                                                            </button>
                                                            <button className="action-btn app-btn-clear">
                                                                <FeatherIcon icon="printer" size="20" /> 
                                                            </button> */}
                                                            <a className="pay-btn" href="javascript:void(0)" >Pay</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <AddLocationModel show={AddLocationModelShow} onHide={() => SetAddLocationModel(false)}
            />
        </div>
    );
}
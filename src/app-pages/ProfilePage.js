import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import DatePicker from "react-datepicker";
import FeatherIcon from 'feather-icons-react';
// component imports
import AppSidebarNavbar from '../common-components/AppSidebarNavbar';
import AddLocationProfileModel from '../models/AddLocationProfileModel';
import EditLocationProfileModel from '../models/EditLocationProfileModel';
import EditUserProfileModel from '../models/EditUserProfileModel';
//images import
import userThumb from "../assets/images/user-thumb-1.png";
//user-thumb-1.png

export default function ProfilePage() {
    const [AddLocationModelShow, SetAddLocationModel,] = React.useState(false);
    const [EditLocationModelShow, SetEditLocationModel,] = React.useState(false);
    const [EditProfileModelShow, SetEditProfileModel,] = React.useState(false);

    return (
        <div className="main-wrapper">
            <AppSidebarNavbar />
            <main>
                <div className="app-container">
                    <nav aria-label="breadcrumb" className="app-breadcrumb req-mar">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active" aria-current="page"> Profile</li>
                        </ol>
                    </nav>
                    <div className="app-content">
                        <div className="profile-page">
                            <div className="white-container d-flex">
                                <div className="user-information">
                                    <div className="inner-container">

                                        <div className="sec-header">
                                            <h4 className="mr-auto">User Information</h4>
                                            <button className="app-btn app-btn-clear edit-btn "  onClick={() => SetEditProfileModel(true)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 17">
                                                    <path fill="#707070" d="M0 12.756v3.27h3.27l9.647-9.648-3.27-3.27L0 12.756zM15.745 2.29L13.709.256a.874.874 0 00-1.234 0L10.88 1.851l3.27 3.27 1.595-1.596a.874.874 0 000-1.234z" />
                                                </svg>
                                            </button>
                                        </div>
                                        <div className="user-img-wrapper">
                                            <div className="user-img" style={{ backgroundImage: `url(${userThumb})` }}>
                                                <div className="upload-btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36.174 36.174">
                                                    <path d="M23.921 20.528c0 3.217-2.617 5.834-5.834 5.834s-5.833-2.617-5.833-5.834 2.616-5.834 5.833-5.834 5.834 2.618 5.834 5.834zm12.253-8.284v16.57a4 4 0 01-4 4H4a4 4 0 01-4-4v-16.57a4 4 0 014-4h4.92V6.86a3.5 3.5 0 013.5-3.5h11.334a3.5 3.5 0 013.5 3.5v1.383h4.92c2.209.001 4 1.792 4 4.001zm-9.253 8.284c0-4.871-3.963-8.834-8.834-8.834-4.87 0-8.833 3.963-8.833 8.834s3.963 8.834 8.833 8.834c4.871 0 8.834-3.963 8.834-8.834z"/>
                                                    </svg>
                                                    <input type="file" />
                                                </div>
                                            </div>
                                            <h4 className="user-name">
                                                Dr. John Jones
                                            </h4>
                                            <h4 className="user-prof">
                                                General Dentist
                                            </h4>
                                        </div>
                                        <div className="user-basic-info">
                                            <div className="info-row">
                                                <span>First Name</span>
                                                <label>John </label>
                                            </div>
                                            <div className="info-row">
                                                <span>Last Name</span>
                                                <label>Doe  </label>
                                            </div>
                                            <div className="info-row">
                                                <span>Specialty</span>
                                                <label>General Dentist </label>
                                            </div>
                                            <div className="info-row">
                                                <span>User Name</span>
                                                <label>azmax </label>
                                            </div>
                                            <div className="info-row">
                                                <span>Email</span>
                                                <label>Johndoe@gmail.com </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="user-location">
                                    <div className="inner-container px-0 pb-0">
                                        <div className="sec-header">
                                            <h4 className="mr-auto">Locations</h4>
                                            <button className="app-btn app-btn-clear add-btn " onClick={() => SetAddLocationModel(true)}>
                                                + Add New Location
                                            </button>
                                        </div>
                                        <div className="location-container border-bottom">
                                            <div className="location-info active">
                                                <button className="app-btn app-btn-clear edit-btn " onClick={() => SetEditLocationModel(true)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 17">
                                                        <path fill="#707070" d="M0 12.756v3.27h3.27l9.647-9.648-3.27-3.27L0 12.756zM15.745 2.29L13.709.256a.874.874 0 00-1.234 0L10.88 1.851l3.27 3.27 1.595-1.596a.874.874 0 000-1.234z" />
                                                    </svg>
                                                </button>
                                                <h4>Watterson Parkway <small>(Primary)</small></h4>
                                                <p>116 Watterson Parkway Trusville,<br /> AL 35173.</p>
                                                <a className="contact-link" href="mailto:test.generaldentist@gmail.com">test.generaldentist@gmail.com</a>
                                                <a className="contact-link" href="tel:(480)9993342"> (480) 999-3342</a>
                                                <p>NPI Number :-</p>
                                            </div>
                                            <div className="location-info">
                                                <button className="app-btn app-btn-clear edit-btn " onClick={() => SetEditLocationModel(true)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 17">
                                                        <path fill="#707070" d="M0 12.756v3.27h3.27l9.647-9.648-3.27-3.27L0 12.756zM15.745 2.29L13.709.256a.874.874 0 00-1.234 0L10.88 1.851l3.27 3.27 1.595-1.596a.874.874 0 000-1.234z" />
                                                    </svg>
                                                </button>
                                                <h4>Birmingham </h4>
                                                <p>116 Watterson Parkway Trusville,<br /> AL 35173.</p>
                                                <a className="contact-link" href="mailto:test.generaldentist@gmail.com">test.generaldentist@gmail.com</a>
                                                <a className="contact-link" href="tel:(480)9993342"> (480) 999-3342</a>
                                                <p>NPI Number :-</p>
                                            </div>
                                        </div>
                                        <div className="location-container second-row">
                                            <div className="location-info">
                                                <button className="app-btn app-btn-clear edit-btn " onClick={() => SetEditLocationModel(true)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 17">
                                                        <path fill="#707070" d="M0 12.756v3.27h3.27l9.647-9.648-3.27-3.27L0 12.756zM15.745 2.29L13.709.256a.874.874 0 00-1.234 0L10.88 1.851l3.27 3.27 1.595-1.596a.874.874 0 000-1.234z" />
                                                    </svg>
                                                </button>
                                                <h4>Atlanka Street</h4>
                                                <p>116 Watterson Parkway Trusville,<br /> AL 35173.</p>
                                                <a className="contact-link" href="mailto:test.generaldentist@gmail.com">test.generaldentist@gmail.com</a>
                                                <a className="contact-link" href="tel:(480)9993342"> (480) 999-3342</a>
                                                <p>NPI Number :-</p>
                                            </div>
                                            <div className="location-info">
                                                <button className="app-btn app-btn-clear edit-btn " onClick={() => SetEditLocationModel(true)}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 17">
                                                        <path fill="#707070" d="M0 12.756v3.27h3.27l9.647-9.648-3.27-3.27L0 12.756zM15.745 2.29L13.709.256a.874.874 0 00-1.234 0L10.88 1.851l3.27 3.27 1.595-1.596a.874.874 0 000-1.234z" />
                                                    </svg>
                                                </button>
                                                <h4>Chandler Arcade</h4>
                                                <p>116 Watterson Parkway Trusville,<br /> AL 35173.</p>
                                                <a className="contact-link" href="mailto:test.generaldentist@gmail.com">test.generaldentist@gmail.com</a>
                                                <a className="contact-link" href="tel:(480)9993342"> (480) 999-3342</a>
                                                <p>NPI Number :-</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <AddLocationProfileModel show={AddLocationModelShow} onHide={() => SetAddLocationModel(false)} />
            <EditLocationProfileModel show={EditLocationModelShow} onHide={() => SetEditLocationModel(false)} />
            <EditUserProfileModel show={EditProfileModelShow} onHide={() => SetEditProfileModel(false)} />
        </div>
    );
}
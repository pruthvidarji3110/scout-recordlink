import React from 'react';
import { NavLink } from 'react-router-dom';
// component imports
import AppSidebarNavbar from '../common-components/AppSidebarNavbar';
//images import
import BrandImg1 from '../assets/images/brand-logo1.png';
import BrandImg2 from '../assets/images/brand-logo2.png';
import BrandImg3 from '../assets/images/brand-logo3.png';
import BrandImg4 from '../assets/images/brand-logo4.png';
import BrandImg5 from '../assets/images/brand-logo5.png';
import BrandImg6 from '../assets/images/brand-logo6.png';
import BrandImg7 from '../assets/images/brand-logo7.png';
import BrandImg8 from '../assets/images/brand-logo8.png';
import ReloadIconBlack from '../assets/images/reload-icon-black.svg';
import NoteEditIcon from '../assets/images/noteEdit-icon-black.svg';
import HIPPAlogo from '../assets/images/HIPAA-logo.png';



export default function IntegrationLocationPage() {
    const activeTabs = { integration: true, Billing: false };
    return (
        <div className="main-wrapper">
            <AppSidebarNavbar activeTabsIntegration={true} />
            <main>
                <div className="app-container">
                    <nav aria-label="breadcrumb" className="app-breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active"><a >Integrations </a></li>
                        </ol>
                    </nav>
                    <p className="header-note">Connecting Scout to your dental practice management system is easy. Simply select your system below and we will walk your through it.</p>
                    <div className="app-content">
                        <div className="row app-box-wrapper">
                            <div className="box-col col-xs-12 col-sm-6 col-lg-6 col-xl-3">
                                <div className="app-box ">
                                    <div className="logo-box d-flex align-items-center justify-content-center">
                                        <img src={BrandImg1} alt="Brand Logo" />
                                    </div>
                                    <div className="btn-box mt-4">
                                        <NavLink exact to="/dentrix" className="btn app-btn  text-uppercase">
                                            Manage
                                        </NavLink>
                                    </div>
                                </div>
                            </div>
                            <div className="box-col col-xs-12 col-sm-6 col-lg-6 col-xl-3">
                                <div className="app-box ">
                                    <div className="logo-box d-flex align-items-center justify-content-center">
                                        <img src={BrandImg2} alt="Brand Logo" />
                                    </div>
                                    <div className="btn-box mt-4">
                                        <button className="btn app-btn gray-btn text-uppercase">
                                            Connect
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="box-col col-xs-12 col-sm-6 col-lg-6 col-xl-3">
                                <div className="app-box ">
                                    <div className="logo-box d-flex align-items-center justify-content-center">
                                        <img src={BrandImg3} alt="Brand Logo" />
                                    </div>
                                    <div className="btn-box mt-4">
                                        <button className="btn app-btn text-uppercase">
                                            Manage
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="box-col col-xs-12 col-sm-6 col-lg-6 col-xl-3">
                                <div className="app-box ">
                                    <div className="logo-box d-flex align-items-center justify-content-center">
                                        <img src={BrandImg4} alt="Brand Logo" />
                                    </div>
                                    <div className="btn-box mt-4">
                                        <button className="btn app-btn text-uppercase">
                                            Manage
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="box-col col-xs-12 col-sm-6 col-lg-6 col-xl-3">
                                <div className="app-box ">
                                    <button className="btn app-btn-clear reload-btn">
                                        <img src={ReloadIconBlack} alt="icon" />
                                    </button>
                                    <div className="logo-box d-flex align-items-center justify-content-center">
                                        <img src={BrandImg5} alt="Brand Logo" />
                                    </div>
                                    <div className="btn-box mt-4">
                                        <button className="btn app-btn text-uppercase">
                                            Manage
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="box-col col-xs-12 col-sm-6 col-lg-6 col-xl-3">
                                <div className="app-box ">
                                    <div className="logo-box d-flex align-items-center justify-content-center">
                                        <img src={BrandImg6} alt="Brand Logo" />
                                    </div>
                                    <div className="btn-box mt-4">
                                        <button className="btn app-btn gray-btn text-uppercase">
                                            Connect
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="box-col col-xs-12 col-sm-6 col-lg-6 col-xl-3">
                                <div className="app-box ">
                                    <div className="logo-box d-flex align-items-center justify-content-center">
                                        <img src={BrandImg7} alt="Brand Logo" />
                                    </div>
                                    <div className="btn-box mt-4">
                                        <button className="btn app-btn gray-btn text-uppercase">
                                            Connect
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="box-col col-xs-12 col-sm-6 col-lg-6 col-xl-3">
                                <div className="app-box ">
                                    <div className="logo-box d-flex align-items-center justify-content-center">
                                        <img src={BrandImg8} alt="Brand Logo" />
                                    </div>
                                    <div className="btn-box mt-4">
                                        <button className="btn app-btn text-uppercase">
                                            Manage
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row app-box-wrapper mt-2">
                            <div className="box-col col-xs-12 col-sm-12 col-lg-12 col-xl-9">
                                <div className="app-box info-box">
                                    <div className="left-box">
                                        <h2 className="info-title">Pay for an expert! </h2>
                                        <p>Need a little assistance with installing the integration tool? No problem! We have experts who can save you time, and get you up and going quickly!</p>
                                    </div>
                                    <div className="right-box">
                                        <div className="amt-info">
                                            <img src={NoteEditIcon} alt="Icon" />
                                            <strong>$149</strong>
                                            <span>One time</span>
                                        </div>
                                        <div className="btn-box">
                                            <button className="btn app-btn small-btn text-uppercase">
                                                Learn More
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="box-col col-xs-12 col-sm-12 col-lg-12 col-xl-3">
                                <div className="hippa-img-box d-flex align-items-center justify-content-center">
                                    <img src={HIPPAlogo} alt="HIPPA Logo" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    );
}